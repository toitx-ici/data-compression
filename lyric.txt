Năm qua tôi đã làm gì cho người sinh ra tôi
Năm qua tôi đã làm gì cho người yêu thương mình
Năm qua tôi đã làm gì cho người nâng đỡ tôi
Và năm qua tôi đã làm gì cho những mối quen thân tình
Giờ là lúc nhìn lại xem một năm vừa trải qua
Buồn vui thế nào có giận hờn có thứ tha
Hài lòng hay thất vọng trưởng thành hơn hay vẫn ngây ngô
Như lúc ban đầu
Dù là như thế nào dù mọi điều đã có ra sao
Chỉ cần ngoảnh lại một nụ cười vẫn nở trên môi
Thành công thất bại chỉ là chuyện năm cũ thôi
Đón chào năm mới
Năm qua tôi có được gì sau những lần rong chơi
Năm qua tôi có được gì sau những bước chân rối bời
Năm qua tôi có được gì sau những lần chia tay
Và năm qua tôi có được gì sau những phút giây quay lại
Giờ là lúc nhìn lại xem một năm vừa trải qua
Buồn vui thế nào có giận hờn có thứ tha
Hài lòng hay thất vọng trưởng thành hơn hay vẫn ngây ngô
Như lúc ban đầu
Dù là như thế nào dù mọi điều đã có ra sao
Chỉ cần ngoảnh lại một nụ cười vẫn nở trên môi
Thành công thất bại chỉ là chuyện năm cũ thôi
Đón chào năm mới
Năm qua tôi có được gì sau những lần rong chơi (có gì rong chơi)
Năm qua tôi có được gì sau những bước chân rối bời (có gì rối bời)
Năm qua tôi có được gì sau những lần chia tay (có gì chia tay)
Và năm qua tôi có được gì sau những phút giây quay lại (có gì sau những phút giây quay lại)
Giờ là lúc nhìn lại xem một năm vừa trải qua
Buồn vui thế nào có giận hờn có thứ tha
Hài lòng hay thất vọng trưởng thành hơn hay vẫn ngây ngô
Như lúc ban đầu (ban đầu ban đầu)
Dù là như thế nào dù mọi điều đã có ra sao
Chỉ cần ngoảnh lại một nụ cười vẫn nở trên môi
Thành công thất bại chỉ là chuyện năm cũ thôi
Đón chào năm mới
Vậy là hết một năm bao bộn bề vừa trải qua
Nhìn xem thế nào có giận hờn có thứ tha
Hài lòng hay thất vọng trưởng thành hơn hay vẫn ngây ngô
Như lúc ban đầu (ban đầu ban đầu)
Dù là như thế nào dù mọi điều đã có ra sao
Chỉ cần ngoảnh lại một nụ cười vẫn nở trên môi
Thành công thất bại chỉ là chuyện năm cũ thôi
Đón chào năm mới
Giờ tôi khép lại thêm một năm nữa đã trôi
Xin chào năm mới
Năm nay tôi sẽ làm gì cho người sinh ra tôi
Năm nay tôi sẽ làm gì cho người yêu thương mình
Năm nay tôi sẽ làm gì cho người nâng đỡ tôi
Và năm nay tôi sẽ làm gì cho những mối quen thân tình