### Data Compression

- Bài toán: Hãy nén file `lyric.txt` thành file có kích thước nhỏ nhất
- Yêu cầu: 
    - Không dùng framework
    - Có thể dùng bất cứ ngôn ngữ lập trình nào
    - File đã nén cần được giải nén về nội dung giống như ban đầu
    - Nộp bài bao gồm:
        - Tên thư mục là tên người dự thi
        - source code bao gồm chương trình nén và chương trình giải nén
        - Có readme.md mô tả về thuật toán của mình hoặc thuật toán mà mình áp dụng
- Cơ cấu giải thưởng:
    - 1 x Giải nhất (200k): file nén có kích thước nhỏ nhất
    - N x Giải khuyến khích (50k): Có submit source code và chạy được đủ vòng `(*)`

***
Giải thích `(*)`

- N là không giới hạn
- Chạy được đủ vòng là: có nén file, file nén có kích thước nhỏ hơn file gốc, và có thể giải nén về file có nội dung giống file gốc